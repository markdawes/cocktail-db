package com.rave.cocktaildb.views.categoryscreen

import com.rave.cocktaildb.model.local.Category

/**
 * Category screen state.
 *
 * @property isLoading
 * @property categories
 * @property error
 * @constructor Create empty Category screen state
 */
data class CategoryScreenState(
    val isLoading: Boolean = false,
    val categories: List<Category> = emptyList(),
    val error: String = "Error encountered"
)
