package com.rave.cocktaildb.views.categorydrinkscreen

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import coil.compose.AsyncImage
import com.rave.cocktaildb.model.local.CategoryDrink
import com.rave.cocktaildb.views.Screens

/**
 * Category drink screen.
 *
 * @param state
 * @param navigate
 * @receiver
 */
@Composable
fun CategoryDrinkScreen(
    state: CategoryDrinkState,
    navigate: (screen: Screens, id: String) -> Unit
) {
    LazyColumn() {
        items(state.categoryDrinks) { drink: CategoryDrink ->
            Row(
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceAround,
                modifier = Modifier.clickable {
                    navigate(Screens.DrinkScreen, drink.idDrink)
                }
            ) {
                AsyncImage(
                    model = drink.strDrinkThumb,
                    contentDescription = "${drink.strDrink} shown as an image."
                )
                Text(text = drink.strDrink)
            }
        }
    }
}
