package com.rave.cocktaildb.views

/**
 * Screens.
 *
 * @property route
 * @constructor Create empty Screens
 */
enum class Screens(val route: String) {
    CategoryScreen("CategoryScreen"),
    CategoryDrinkScreen("CategoryDrinkScreen"),
    DrinkScreen("DrinkScreen")
}
