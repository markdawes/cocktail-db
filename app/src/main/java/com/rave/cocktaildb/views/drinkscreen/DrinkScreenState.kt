package com.rave.cocktaildb.views.drinkscreen

import com.rave.cocktaildb.model.local.Drink

/**
 * Drink screen state.
 *
 * @property isLoading
 * @property drinks
 * @property error
 * @constructor Create empty Drink screen state
 */
data class DrinkScreenState(
    val isLoading: Boolean = false,
    val drinks: List<Drink> = emptyList(),
    val error: String = "Error encountered"
)
