package com.rave.cocktaildb.views

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.rave.cocktaildb.model.CocktailRepo
import com.rave.cocktaildb.model.local.Category
import com.rave.cocktaildb.model.mapper.CategoryDrinkMapper
import com.rave.cocktaildb.model.mapper.CategoryMapper
import com.rave.cocktaildb.model.mapper.DrinkMapper
import com.rave.cocktaildb.model.remote.RetrofitObject
import com.rave.cocktaildb.ui.theme.CocktailDBTheme
import com.rave.cocktaildb.viewmodel.DrinkViewModel
import com.rave.cocktaildb.viewmodel.VMlFactory
import com.rave.cocktaildb.views.categorydrinkscreen.CategoryDrinkScreen
import com.rave.cocktaildb.views.categorydrinkscreen.CategoryDrinkState
import com.rave.cocktaildb.views.categoryscreen.CategoryScreen
import com.rave.cocktaildb.views.categoryscreen.CategoryScreenState
import com.rave.cocktaildb.views.drinkscreen.DrinkScreen
import com.rave.cocktaildb.views.drinkscreen.DrinkScreenState

/**
 * Main activity.
 *
 * @constructor Create empty Main activity
 */
class MainActivity : ComponentActivity() {

    private val drinkViewModel by viewModels<DrinkViewModel> {
        val repo = CocktailRepo(
            RetrofitObject.getAPIService(),
            CategoryMapper(),
            CategoryDrinkMapper(),
            DrinkMapper()
        )
        VMlFactory(repo)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        drinkViewModel.getCategories()
        setContent {
            val categoryState by drinkViewModel.categoryState.collectAsState()
            val categoryDrinkState by drinkViewModel.categoryDrinkState.collectAsState()
            val drinkScreenState by drinkViewModel.drinkScreenState.collectAsState()
            CocktailDBTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    DrinkListApp(
                        categoryState,
                        categoryDrinkState,
                        drinkScreenState,
                        onCategorySelected = { selectedCategory: Category ->
                            drinkViewModel.getDrinksInCategory(selectedCategory)
                        },
                        onDrinkSelected = { drinkId: String ->
                            drinkViewModel.getDrinkFromId(drinkId)
                        }
                    )
                }
            }
        }
    }
}

@Composable
fun DrinkListApp(
    categories: CategoryScreenState,
    categoryDrinkState: CategoryDrinkState,
    drinkScreenState: DrinkScreenState,
    onCategorySelected: (category: Category) -> Unit,
    onDrinkSelected: (id: String) -> Unit
) {
    val navController: NavHostController = rememberNavController()
    NavHost(
        navController = navController,
        startDestination = Screens.CategoryScreen.route
    ) {
        composable(Screens.CategoryScreen.route) {
            CategoryScreen(categories) { screen: Screens, category: Category ->
                onCategorySelected(category)
                navController.navigate(screen.route)
            }
        }
        composable(Screens.CategoryDrinkScreen.route) {
            CategoryDrinkScreen(categoryDrinkState) { screen: Screens, id: String ->
                onDrinkSelected(id)
                navController.navigate(screen.route)
            }
        }
        composable(Screens.DrinkScreen.route) {
            DrinkScreen(drinkScreenState)
        }
    }
}
