package com.rave.cocktaildb.views.categoryscreen

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp
import com.rave.cocktaildb.model.local.Category
import com.rave.cocktaildb.views.Screens

/**
 * Category screen.
 *
 * @param state
 * @param navigate
 * @receiver
 */
@Composable
fun CategoryScreen(
    state: CategoryScreenState,
    navigate: (screen: Screens, category: Category) -> Unit
) {
    LazyColumn(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.SpaceEvenly,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        items(state.categories) { category: Category ->
            Row(
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceAround,
                modifier = Modifier.clickable {
                    navigate(Screens.CategoryDrinkScreen, category)
                }
            ) {
                Text(
                    text = category.strCategory,
                    // modifier = Modifier.size(150.dp),
                    style = TextStyle(
                        fontWeight = FontWeight.SemiBold,
                        fontSize = 30.sp
                    )
                )
            }
        }
    }
}
