package com.rave.cocktaildb.views.drinkscreen

import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import coil.compose.AsyncImage

/**
 * Drink screen.
 *
 * @param state
 */
@Composable
fun DrinkScreen(
    state: DrinkScreenState
) {
    for (drink in state.drinks) {
        Text(text = "Details for ${drink.strDrink}")
        Text(text = "\nDrink is ${drink.strAlcoholic}")
        Text(text = "\n\nDrink is in category ${drink.strCategory}")
        AsyncImage(
            model = "${drink.strImageSource}",
            contentDescription = "\n\n\nImage for ${drink.strDrink}"
        )
    }
}
