package com.rave.cocktaildb.model.mapper

import com.rave.cocktaildb.model.local.Drink
import com.rave.cocktaildb.model.remote.dtos.drink.DrinkDTO

/**
 * Drink mapper.
 *
 * @constructor Create empty Drink mapper
 */
class DrinkMapper : Mapper<DrinkDTO, Drink> {
    override fun invoke(dto: DrinkDTO): Drink = with(dto) {
        Drink(
            dateModified = dateModified,
            idDrink = idDrink,
            strAlcoholic = strAlcoholic,
            strCategory = strCategory,
            strCreativeCommonsConfirmed = strCreativeCommonsConfirmed,
            strDrink = strDrink,
            strDrinkAlternate = strDrinkAlternate,
            strDrinkThumb = strDrinkThumb,
            strGlass = strGlass,
            strIBA = strIBA,
            strImageAttribution = strImageAttribution,
            strImageSource = strImageSource,
            strTags = strTags,
            strVideo = strVideo
        )
    }
}
