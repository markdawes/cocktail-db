package com.rave.cocktaildb.model.mapper

import com.rave.cocktaildb.model.local.CategoryDrink
import com.rave.cocktaildb.model.remote.dtos.categorydrink.CategoryDrinkDTO

/**
 * Category drink mapper.
 *
 * @constructor Create empty Category drink mapper
 */
class CategoryDrinkMapper : Mapper<CategoryDrinkDTO, CategoryDrink> {
    override fun invoke(dto: CategoryDrinkDTO): CategoryDrink = with(dto) {
        CategoryDrink(idDrink, strDrink, strDrinkThumb)
    }
}
