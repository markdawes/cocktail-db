package com.rave.cocktaildb.model.mapper

import com.rave.cocktaildb.model.local.Category
import com.rave.cocktaildb.model.remote.dtos.category.CategoryDTO

/**
 * Category mapper.
 *
 * @constructor Create empty Category mapper
 */
class CategoryMapper : Mapper<CategoryDTO, Category> {
    override fun invoke(dto: CategoryDTO): Category = with(dto) {
        Category(
            strCategory = strCategory
        )
    }
}
