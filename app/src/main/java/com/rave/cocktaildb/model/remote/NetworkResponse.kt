package com.rave.cocktaildb.model.remote

import com.rave.cocktaildb.model.local.Drink

/**
 * Network response.
 *
 * @param T
 * @constructor Create empty Network response
 */
sealed class NetworkResponse<T> {
    /**
     * Success.
     *
     * @param T
     * @property successList
     * @constructor Create empty Success
     */
    sealed class Success<T>(
        val successList: List<T>
    ) : NetworkResponse<List<T>>() {

        /**
         * Category success.
         *
         * @param Category
         * @property categoryList
         * @constructor Create empty Category success
         */
        data class CategorySuccess<Category>(val categoryList: List<Category>) :
            Success<Category>(categoryList)

        /**
         * Category drink success.
         *
         * @param CategoryDrink
         * @property drinksInCategory
         * @constructor Create empty Category drink success
         */
        data class CategoryDrinkSuccess<CategoryDrink>(val drinksInCategory: List<CategoryDrink>) :
            Success<CategoryDrink>(drinksInCategory)

        /**
         * Drink success.
         *
         * @param Drink
         * @property drinks
         * @constructor Create empty Drink success
         */
        data class DrinkSuccess<Drink>(val drinks: List<Drink>) :
            Success<Drink>(drinks)
    }

    /**
     * Error.
     *
     * @property message
     * @constructor Create empty Error
     */
    data class Error(val message: String) : NetworkResponse<String>()
}
