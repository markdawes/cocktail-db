package com.rave.cocktaildb.model.remote.dtos.category

import kotlinx.serialization.Serializable

/**
 * Category response.
 *
 * @property drinks
 * @constructor Create empty Category response
 */
@Serializable
data class CategoryResponse(
    val drinks: List<CategoryDTO> = emptyList()
)
