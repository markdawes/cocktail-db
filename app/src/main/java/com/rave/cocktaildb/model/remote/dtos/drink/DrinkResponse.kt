package com.rave.cocktaildb.model.remote.dtos.drink

import kotlinx.serialization.Serializable

/**
 * Drink response.
 *
 * @property drinks
 * @constructor Create empty Drink response
 */
@Serializable
data class DrinkResponse(
    val drinks: List<DrinkDTO>
)
