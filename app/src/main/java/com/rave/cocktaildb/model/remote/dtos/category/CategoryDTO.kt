package com.rave.cocktaildb.model.remote.dtos.category

import kotlinx.serialization.Serializable

/**
 * Category dto.
 *
 * @property strCategory
 * @constructor Create empty Category d t o
 */
@Serializable
data class CategoryDTO(
    val strCategory: String
)
