package com.rave.cocktaildb.model

import com.rave.cocktaildb.model.local.Category
import com.rave.cocktaildb.model.mapper.CategoryDrinkMapper
import com.rave.cocktaildb.model.mapper.CategoryMapper
import com.rave.cocktaildb.model.mapper.DrinkMapper
import com.rave.cocktaildb.model.remote.APIService
import com.rave.cocktaildb.model.remote.NetworkResponse
import com.rave.cocktaildb.model.remote.dtos.category.CategoryResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response

/**
 * Cocktail repo.
 *
 * @property service
 * @property mapper
 * @property categoryDrinkMapper
 * @property drinkMapper
 * @constructor Create empty Cocktail repo
 */
class CocktailRepo(
    private val service: APIService,
    private val mapper: CategoryMapper,
    private val categoryDrinkMapper: CategoryDrinkMapper,
    private val drinkMapper: DrinkMapper
) {
    /**
     * Get drink categories.
     *
     * @return
     */
    suspend fun getDrinkCategories(): NetworkResponse<*> = withContext(Dispatchers.IO) {
        val mealResponse: Response<CategoryResponse> =
            service.getAllDrinkCategories().execute()

        return@withContext if (mealResponse.isSuccessful) {
            val categoryResponse = mealResponse.body() ?: CategoryResponse()
            val categoryList: List<Category> = categoryResponse.drinks
                .map { mapper(it) }
            NetworkResponse.Success.CategorySuccess(categoryList)
        } else {
            NetworkResponse.Error(mealResponse.message())
        }
    }

    /**
     * Get drink in category.
     *
     * @param category
     * @return
     */
    suspend fun getDrinkInCategory(category: Category): NetworkResponse<*> =
        withContext(Dispatchers.IO) {
            val categoryDrinkResponse = service.getDrinkFromCategory(category.strCategory).execute()
            if (categoryDrinkResponse.isSuccessful) {
                val drinksInCategoryResponse =
                    categoryDrinkResponse.body()?.drinks ?: emptyList()
                NetworkResponse.Success.CategoryDrinkSuccess(
                    drinksInCategoryResponse.map { categoryDrinkMapper(it) }
                )
            } else {
                NetworkResponse.Error(categoryDrinkResponse.message())
            }
        }

    /**
     * Get drink from id.
     *
     * @param id
     * @return
     */
    suspend fun getDrinkFromId(id: String): NetworkResponse<*> =
        withContext(Dispatchers.IO) {
            val drinkResponse = service.getDrink(id).execute()
            if (drinkResponse.isSuccessful) {
                val drinksInResponse =
                    drinkResponse.body()?.drinks ?: emptyList()
                NetworkResponse.Success.DrinkSuccess(
                    drinksInResponse.map { drinkMapper(it) }
                )
            } else {
                NetworkResponse.Error(drinkResponse.message())
            }
        }
}
