package com.rave.cocktaildb.model.remote.dtos.categorydrink

import kotlinx.serialization.Serializable

/**
 * Category drink dto.
 *
 * @property idDrink
 * @property strDrink
 * @property strDrinkThumb
 * @constructor Create empty Category drink d t o
 */
@Serializable
data class CategoryDrinkDTO(
    val idDrink: String,
    val strDrink: String,
    val strDrinkThumb: String
)
