package com.rave.cocktaildb.model.local

/**
 * Drink.
 *
 * @property dateModified
 * @property idDrink
 * @property strAlcoholic
 * @property strCategory
 * @property strCreativeCommonsConfirmed
 * @property strDrink
 * @property strDrinkAlternate
 * @property strDrinkThumb
 * @property strGlass
 * @property strIBA
 * @property strImageAttribution
 * @property strImageSource
 * @property strTags
 * @property strVideo
 * @constructor Create empty Drink
 */
data class Drink(
    val dateModified: String?,
    val idDrink: String?,
    val strAlcoholic: String?,
    val strCategory: String?,
    val strCreativeCommonsConfirmed: String?,
    val strDrink: String?,
    val strDrinkAlternate: String?,
    val strDrinkThumb: String?,
    val strGlass: String?,
    val strIBA: String?,
    val strImageAttribution: String?,
    val strImageSource: String?,
    val strTags: String?,
    val strVideo: String?
)
