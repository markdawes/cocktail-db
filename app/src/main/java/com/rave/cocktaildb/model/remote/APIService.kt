package com.rave.cocktaildb.model.remote

import com.rave.cocktaildb.model.remote.dtos.category.CategoryResponse
import com.rave.cocktaildb.model.remote.dtos.categorydrink.CategoryDrinkResponse
import com.rave.cocktaildb.model.remote.dtos.drink.DrinkResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Api service.
 *
 * @constructor Create empty Api service
 */
interface APIService {

    @GET(CATEGORY_ENDPOINT)
    fun getAllDrinkCategories(): Call<CategoryResponse>

    @GET(DRINK_IN_CATEGORY_ENDPOINT)
    fun getDrinkFromCategory(@Query("c") categoryName: String): Call<CategoryDrinkResponse>

    @GET(DRINK_ENDPOINT)
    fun getDrink(@Query("i") drinkId: String): Call<DrinkResponse>

    companion object {
        private const val CATEGORY_ENDPOINT = "list.php?c=list"
        private const val DRINK_IN_CATEGORY_ENDPOINT = "filter.php"
        private const val DRINK_ENDPOINT = "lookup.php"
    }
}
