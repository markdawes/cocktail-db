package com.rave.cocktaildb.model.mapper

/**
 * Mapper.
 *
 * @param DTO
 * @param ENTITY
 * @constructor Create empty Mapper
 */
interface Mapper<in DTO, out ENTITY> {
    /**
     * Invoke.
     *
     * @param dto
     * @return
     */
    operator fun invoke(dto: DTO): ENTITY
}
