package com.rave.cocktaildb.model.remote.dtos.categorydrink

import kotlinx.serialization.Serializable

/**
 * Category drink response.
 *
 * @property drinks
 * @constructor Create empty Category drink response
 */
@Serializable
data class CategoryDrinkResponse(
    val drinks: List<CategoryDrinkDTO>
)
