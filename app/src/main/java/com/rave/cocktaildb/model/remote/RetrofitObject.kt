package com.rave.cocktaildb.model.remote

import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import retrofit2.Retrofit
import retrofit2.create

/**
 * Retrofit object.
 *
 * @constructor Create empty Retrofit object
 */
@OptIn(ExperimentalSerializationApi::class)
object RetrofitObject {
    private const val VERSION = "/api/json/v1/1/"
    private const val BASE_URL = "https://www.thecocktaildb.com$VERSION"

    private val mediaType = "application/json".toMediaType()

    private val retrofit: Retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(Json.asConverterFactory(mediaType))
        .build()

    /**
     * Get api service.
     *
     * @return
     */
    fun getAPIService(): APIService = retrofit.create()
}
