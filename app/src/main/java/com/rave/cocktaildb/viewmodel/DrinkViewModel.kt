package com.rave.cocktaildb.viewmodel

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.rave.cocktaildb.model.CocktailRepo
import com.rave.cocktaildb.model.local.Category
import com.rave.cocktaildb.model.local.CategoryDrink
import com.rave.cocktaildb.model.local.Drink
import com.rave.cocktaildb.model.remote.NetworkResponse
import com.rave.cocktaildb.views.categorydrinkscreen.CategoryDrinkState
import com.rave.cocktaildb.views.categoryscreen.CategoryScreenState
import com.rave.cocktaildb.views.drinkscreen.DrinkScreenState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

/**
 * Drink view model.
 *
 * @property repo
 * @constructor Create empty Drink view model
 */
class DrinkViewModel(
    private val repo: CocktailRepo
) : ViewModel() {
    private val tag = "DrinkViewModel"

    private val _categories: MutableStateFlow<CategoryScreenState> =
        MutableStateFlow(CategoryScreenState())
    val categoryState: StateFlow<CategoryScreenState> get() = _categories

    private val _categoryDrinkState: MutableStateFlow<CategoryDrinkState> =
        MutableStateFlow(CategoryDrinkState())
    val categoryDrinkState: StateFlow<CategoryDrinkState> get() = _categoryDrinkState

    private val _drinkScreenState: MutableStateFlow<DrinkScreenState> =
        MutableStateFlow(DrinkScreenState())
    val drinkScreenState: StateFlow<DrinkScreenState> get() = _drinkScreenState

    /**
     * Get categories.
     *
     */
    fun getCategories() = viewModelScope.launch {
        _categories.update { it.copy(isLoading = true) }
        val categories = repo.getDrinkCategories()
        handleResults(categories)
    }

    /**
     * Get drinks in category.
     *
     * @param category
     */
    fun getDrinksInCategory(category: Category) = viewModelScope.launch {
        _categoryDrinkState.update { it.copy(isLoading = true) }
        val categoryDrinks = repo.getDrinkInCategory(category)
        handleResults(categoryDrinks)
    }

    /**
     * Get drink from id.
     *
     * @param id
     */
    fun getDrinkFromId(id: String) = viewModelScope.launch {
        _drinkScreenState.update { it.copy(isLoading = true) }
        val drinks = repo.getDrinkFromId(id)
        handleResults(drinks)
    }

    /**
     * Handle results.
     *
     * @param networkResponse
     */
    private fun handleResults(networkResponse: NetworkResponse<*>) {
        when (networkResponse) {
            is NetworkResponse.Error -> _categories.update {
                it.copy(
                    isLoading = false,
                    error = networkResponse.message
                )
            }
            is NetworkResponse.Success.CategorySuccess<*> -> _categories.update {
                it.copy(
                    isLoading = false,
                    categories = networkResponse.categoryList as List<Category>
                )
            }
            is NetworkResponse.Success.CategoryDrinkSuccess<*> -> _categoryDrinkState.update {
                it.copy(
                    isLoading = false,
                    categoryDrinks = networkResponse.drinksInCategory as List<CategoryDrink>
                )
            }
            is NetworkResponse.Success.DrinkSuccess<*> -> _drinkScreenState.update {
                it.copy(
                    isLoading = false,
                    drinks = networkResponse.drinks as List<Drink>
                )
            }
            else -> Log.e(tag, "Error encountered")
        }
    }
}

/**
 * Vml factory.
 *
 * @property repo
 * @constructor Create empty Vml factory
 */
class VMlFactory(
    private val repo: CocktailRepo
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(DrinkViewModel::class.java)) {
            return DrinkViewModel(repo) as T
        } else {
            @Suppress("UseRequire")
            throw IllegalArgumentException("Illegal Argument Exception")
        }
    }
}
